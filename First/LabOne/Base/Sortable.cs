﻿using System;
using System.Collections.Generic;
using System.Text;
using LabOne.Interfaces;
using LabOne.Extensions;

namespace LabOne.Base
{
    public class Sortable<T> : IMergeSortable<T>, IInsertionSortable<T> where T : CollectionBase, IDataStructure
    {
        public void MergeSort(T structure)
        {
            MergeSort(structure, new int[structure.Length], 0, structure.Length -1);
        }

        private void MergeSort(T structure, int[] temp, int leftStart, int rightEnd)
        {
            if (leftStart >= rightEnd)
                return;
            var middle = (leftStart + rightEnd) / 2;
            MergeSort(structure, temp, leftStart, middle);
            MergeSort(structure, temp, middle + 1, rightEnd);
            MergeHalves(structure, temp, leftStart, rightEnd);
        }

        private void MergeHalves(T structure, int[] temp, int leftStart, int rightEnd)
        {
            int leftEnd = (rightEnd + leftStart) / 2;
            int rightStart = leftEnd + 1;
            int size = rightEnd - leftStart + 1;

            int left = leftStart;
            int right = rightStart;
            int index = leftStart;

            while (left <= leftEnd && right <= rightEnd)
            {
                if (structure[left] <= structure[right])
                {
                    temp[index] = structure[left];
                    left++;
                }
                else
                {
                    temp[index] = structure[right];
                    right++;
                }
                index++;
            }
            CopyData(structure, left, temp, index, leftEnd - left + 1);
            CopyData(structure, right, temp, index, rightEnd - right + 1);
            CopyData(temp, leftStart, structure, leftStart, size);
        }

        private void CopyData(T src, int srcStartIndex, int[] to, int toStartIndex, int amountOfElements)
        {
            for (int i = 0; i < amountOfElements; i++)
            {
                to[toStartIndex++] = src[srcStartIndex++];
            }
        }

        private void CopyData(int[] src, int srcStartIndex, T to, int toStartIndex, int amountOfElements)
        {
            for (int i = 0; i < amountOfElements; i++)
            {
                to[toStartIndex++] = src[srcStartIndex++];
            }
        }

        public void InsertionSort(T collection)
        {
            for (int i = 1; i < collection.Length; i++)
            {
                var value = collection[i];
                var flag = false;
                for (int j = i - 1; j >= 0 && flag != true;)
                {
                    if (value < collection[j])
                    {
                        collection[j + 1] = collection[j];
                        j--;
                        collection[j + 1] = value;
                    }
                    else
                    {
                        flag = true;
                    }
                }
            }
        }

        public void HeapSort(T collection)
        {
            
        }

        private void MaxHeap(T collection, int parentIndex)
        {
            int leftChild = parentIndex * 2 + 1;
            int rightChild = parentIndex * 2 + 1;
            if (collection.Length < leftChild)
                return;
            
            
            var tmp = 0;
            if (collection[leftChild] > collection[rightChild] && collection[parentIndex] > collection[leftChild])
            {
                tmp = collection[leftChild];
                collection[leftChild] = collection[parentIndex];
                collection[parentIndex] = tmp;
                MaxHeap(collection, leftChild);
            }

            if (collection[rightChild] >= collection[leftChild] && collection[parentIndex] > collection[rightChild])
            {
                tmp = collection[rightChild];
                collection[rightChild] = collection[parentIndex];
                collection[parentIndex] = tmp;
                MaxHeap(collection, rightChild);
            }
           
        }

        public void QuickSort(T collection)
        {
            QuickSort(collection, 0, collection.Length);
        }

        private void QuickSort(T collection, int fromIndex, int toIndex)
        {
            if (fromIndex <= toIndex)
            {
                int j = Partition(collection, fromIndex);
                QuickSort(collection, fromIndex, j);
                QuickSort(collection, j + 1, toIndex);

            }
        }

        private int Partition(T collection, int fromIndex)
        {
            var pivot = collection[fromIndex];
            int i = 0;
            int j = collection.Length;

            while (i < j)
            {
                while (collection[i] <= pivot)
                    i++;
                while (collection[j] > pivot)
                    j--;
                if (i < j)
                {
                    var tmp = collection[i];
                    collection[i] = collection[j];
                    collection[j] = tmp;
                }
            }
            return j;
        }


    }
}
