﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LabOne.Base
{
    public abstract class CollectionBase
    {
        public int BYTES { get; } = 4;
        public abstract int this[int index] { get; set; }
        public abstract int Length { get;}
    }
}
