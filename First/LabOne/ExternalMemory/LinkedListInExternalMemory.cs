﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LabOne.Interfaces;
using CollectionBase = LabOne.Base.CollectionBase;

namespace LabOne.ExternalMemory
{
    public class LinkedListInExternalMemory : CollectionBase, IDataStructure, IDisposable, IEnumerable
    {
        private Node Head { get; set; }
        private Node Current;
        private readonly ISeedService _seedService;
        private FileStream _fs { get; }
        private int SizeOfList = 0;
        public override int Length => SizeOfList;
        private int CurrentLastIndex { get; set; } = 0;
        public override int this[int index]
        {
            get => GetAtIndex(index);
            set => SetAtIndex(index, value);
        }

        public LinkedListInExternalMemory(FileStream fileStream, ISeedService seed, int[] initialData = null)
        {
            _fs = fileStream;
            _seedService = seed;
            Fill(initialData);
        }

        ~LinkedListInExternalMemory()
        {
            this.Dispose();
        }

        public IEnumerator GetEnumerator()
        {
            ResetCurrent();
            while (Head != null && Current.Next != null)
            {
                yield return Current.Data;
                Current = Current.Next;
            }
        }

        private void Fill(int[] initialData = null)
        {
            var data = initialData ?? this._seedService.GenerateAndReturnData().ToArray();
            foreach (var i in data)
            {
                Insert(i);
            }

            UpdateNodeCounts();
        }

        public void Insert(int item)
        {
            var node = new Node(item, _fs) {Index = CurrentLastIndex++};
            this.SizeOfList++;
            if (Head == null)
            {
                Head = node;
                Current = Head;
            }
            else
            {
                Current.Next = node;
                Current = Current.Next;
            }

            UpdateNodeCounts();
        }

        private void UpdateNodeCounts()
        {
            ResetCurrent();
            while (Current.Next != null)
            {
                Current.TotalNodes = SizeOfList;
                Current = Current.Next;
            }
            Current.TotalNodes = SizeOfList;
        }

        private void ResetCurrent()
        {
            Current = Head;
        }

        public List<int> ToList()
        {
            ResetCurrent();
            var list = new List<int>();
            while (Current.Next != null)
            {
                list.Add(Current.Data);
                Current = Current.Next;
            }
            list.Add(Current.Data);
            return list;
        }

        public int GetAtIndex(int index)
        {
            ResetCurrent();
            for (int i = 0; Current.Next != null; i++)
            {
                if (i == index)
                    return Current.Data;
                Current = Current.Next;
            }

            return Current.Data;
        }

        public void SetAtIndex(int index, int data)
        {
            ResetCurrent();
            for (int i = 0; Current.Next != null; i++)
            {
                if (i == index)
                    Current.Data = data;
                Current = Current.Next;
            }

            if (index == this.Length - 1)
            {
                Current.Data = data;
            }
        }


        public void Swap(int fromIndex, int toIndex)
        {
            if (fromIndex == toIndex)
                throw new Exception("Indexes must not be the same");
            ResetCurrent();
            int index = 0;
            Node first = null;
            Node second = null;
            while (Current.Next != null)
            {
                if (index == fromIndex)
                    first = Current;
                if (index == toIndex)
                    second = Current;
                Current = Current.Next;
                index++;
            }
            SwapNodes(first, second);
        }

        private void SwapNodes(Node from, Node to)
        {
            var tmp = from.Data;
            from.Data = to.Data;
            to.Data = tmp;
        }

        private class Node
        {
            public int BYTES = 4;
            public int Index { get; set; }
            public int TotalNodes { get; set; }
            public Node Next { get; set; }
            private FileStream Fs { get; }
            public int Data
            {
                get
                {
                    var data = new Byte[TotalNodes * BYTES];

                    Fs.Seek(BYTES * Index, SeekOrigin.Begin);
                    Fs.Read(data, 0, TotalNodes * BYTES);

                    return BitConverter.ToInt32(data, 0);
                }
                set
                {
                    var data = new Byte[BYTES];

                    BitConverter.GetBytes(value).CopyTo(data, 0);

                    Fs.Seek(BYTES * Index, SeekOrigin.Begin);
                    Fs.Write(data, 0, BYTES);
                }
            }

            public Node(int data, FileStream fs)
            {
                Fs = fs;
                Data = data;
            }
        }

        private void ReleaseUnmanagedResources()
        {
            // TODO release unmanaged resources here
        }

        private void Dispose(bool disposing)
        {
            ReleaseUnmanagedResources();
            if (disposing)
            {
                _fs?.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
