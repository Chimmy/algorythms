﻿using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Text;
using LabOne.Interfaces;
using CollectionBase = LabOne.Base.CollectionBase;

namespace LabOne.ExternalMemory
{
    public class ArrayStructureInExternalMemory : CollectionBase, IDataStructure, IDisposable, IEnumerable
    {
        private readonly ISeedService _seedService;
        public int DataEntryCount;
        public override int Length => this.DataEntryCount;
        private FileStream _fs;

        public ArrayStructureInExternalMemory(ISeedService seedService, FileStream fs, int[] initialData = null)
        {
            _seedService = seedService;
            _fs = fs;
            Fill(initialData);
        }

        ~ArrayStructureInExternalMemory()
        {
            Dispose();
        }

        public IEnumerator GetEnumerator()
        {
            foreach (var item in this)
                yield return item;
        }

        private void Fill(int[] initialData = null)
        {
            var data = initialData ?? this._seedService.GenerateAndReturnData().ToArray();
            DataEntryCount = data.Length;
            var dataString = string.Join(Environment.NewLine, data.Select(x => x.ToString()));
            byte[] bytes = Encoding.Default.GetBytes(dataString);
            _fs.Write(bytes);
            _fs.Flush();
        }

        public int[] GetData()
        {
            var dataList = new int[DataEntryCount];
            var currentIndex = 0;
            while (currentIndex < DataEntryCount)
                dataList[currentIndex] = this[currentIndex++];
            return dataList;
        }

        public override int this[int index]
        {
            get
            {
                Byte[] data = new Byte[DataEntryCount * BYTES];

                _fs.Seek(BYTES * index, SeekOrigin.Begin);
                _fs.Read(data, 0, DataEntryCount * BYTES);

                return BitConverter.ToInt32(data, 0);
            }
            set
            {
                Byte[] data = new Byte[BYTES];

                BitConverter.GetBytes(value).CopyTo(data, 0);

                _fs.Seek(BYTES * index, SeekOrigin.Begin);
                _fs.Write(data, 0, BYTES);
            }
        }

        public void Swap(int indexOne, int indexTwo)
        {
            var tmp = this[indexOne];
            this[indexOne] = this[indexTwo];
            this[indexTwo] = tmp;
        }

        private void ReleaseUnmanagedResources()
        {
            // TODO release unmanaged resources here
        }

        private void Dispose(bool disposing)
        {
            ReleaseUnmanagedResources();
            if (disposing)
            {
                _fs?.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}