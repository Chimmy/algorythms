﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LabOne.Interfaces;
using Microsoft.Extensions.Configuration;

namespace LabOne
{
    public class SeedService : ISeedService
    {
        private readonly IConfigurationRoot _config;

        public SeedService(IConfigurationRoot config)
        {
            _config = config;
        }
        public IList<int> GenerateAndReturnData(int? specifiedDataCount = null)
        {
            int dataCount;
            int.TryParse(this._config["amountOfDataToGenerate"], out dataCount);
            dataCount = dataCount == 0 ? 10 : dataCount;
            dataCount = specifiedDataCount ?? dataCount;
            int seed = (int) DateTime.Now.Ticks & 0x0000FFFF;
            Random rand = new Random(seed);
            var list = new List<int>();
            for (int i = 0; i < dataCount; i++)
            {
                list.Add(rand.Next());
            }
            return list; 
        }
    }
}


