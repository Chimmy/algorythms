﻿using System;
using System.Collections.Generic;
using System.Text;
using LabOne.Interfaces;

namespace LabOne._4
{
    public class ProfitCalculator : IProfitCalculator
    {
        private ISeedService _seedService;
        public ProfitCalculator(ISeedService seedService)
        {
            _seedService = seedService;
        }

        public int MaxProfit(int k, params int[] price)
        {
            int n = price.Length;
            int[, ] profit = new int[k + 1, n + 1];

            for (int i = 0; i <= k; i++)
            {
                profit[i, 0] = 0;
            }

            for (int i = 1; i <= k; i++)
            {
                for (int j = 1; j < n; j++)
                {
                    int maxSoFar = 0;

                    for (int m = 0; m < j; m++)
                    {
                        maxSoFar = Math.Max(maxSoFar, price[j] - price[m] + profit[i - 1, m]);
                    }
                    profit[i, j] = Math.Max(profit[i, j - 1], maxSoFar);
                }
            }

            return profit[k, n - 1];
        }
    }

    public interface IProfitCalculator
    {
        int MaxProfit(int k, params int[] price);
    }
}
