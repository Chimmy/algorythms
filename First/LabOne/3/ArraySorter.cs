﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using LabOne.Interfaces;

namespace LabOne._3
{
    public class ArraySorter : IArraySorter
    {
        private readonly ISeedService _seedService;
        public ArraySorter(ISeedService seedService)
        {
            _seedService = seedService;
        }

        public int[][] SortArraysInSync(int mainArrayLength = 0, int subArrayLength = 0)
        {
            subArrayLength = subArrayLength == 0 ? 10 : subArrayLength;
            mainArrayLength = mainArrayLength == 0 ? 10 : mainArrayLength;

            int[][] construct = new int[mainArrayLength][];

            InitializeConstruct(construct, mainArrayLength, subArrayLength);
            for (int i = 0; i < mainArrayLength; i++)
            {
                Array.Sort(construct[i]);
            }

            return construct;
        }

        public int[][] SortArraysAsync(int mainArrayLength = 0, int subArrayLength = 0)
        {
            subArrayLength = subArrayLength == 0 ? 10 : subArrayLength;
            mainArrayLength = mainArrayLength == 0 ? 10 : mainArrayLength;

            int[][] construct = new int[mainArrayLength][];
            InitializeConstruct(construct, mainArrayLength, subArrayLength);
            Parallel.For(0, mainArrayLength, index => { Array.Sort(construct[index]);});

            return construct;
        }

        public Task<int[]> SortListAsync(int[] arrayToSort)
        {
            return new Task<int[]>(() =>
            {
                Array.Sort(arrayToSort);
                return arrayToSort;
            });
        }

        private void InitializeConstruct(int[][] construct, int mainArrayLength, int subArrayLength)
        {
            for (int i = 0; i < mainArrayLength; i++)
            {
                construct[i] = _seedService.GenerateAndReturnData(subArrayLength).ToArray();
            }
        }
    }

    public interface IArraySorter
    {
        int[][] SortArraysInSync(int mainArrayLength = 0, int subArrayLength = 0);
        int[][] SortArraysAsync(int mainArrayLength = 0, int subArrayLength = 0);
    }

}
