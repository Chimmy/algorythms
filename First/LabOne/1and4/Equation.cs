﻿using System;
using System.Collections.Generic;
using System.Text;
using LabOne.Interfaces;

namespace LabOne._1and4
{
    public class Equation
    {
        private Dictionary<int, int> caseDictionary;
        public Equation()
        {
            caseDictionary = new Dictionary<int, int>();
        }

        public int GetValueRecursive(int val)
        {
            if (val == 1)
            {
                return val;
            }

            val--;
            var mostInnerRecursion = GetValueRecursive(val);
            var middleRecursion = GetValueRecursive(mostInnerRecursion);
            var lastRecursion = GetValueRecursive(val + 1 - middleRecursion);
            return 1 + lastRecursion;
        }

        public int GetValueDynamic(int val)
        {
            if (val == 1)
            {
                return val;
            }

            val--;
            var mostInnerRecursion = 0;
            var middleRecursion = 0;
            var lastRecursion = 0;

            if (this.caseDictionary.TryGetValue(val, out mostInnerRecursion))
            {
                mostInnerRecursion = GetValueDynamic(val);
                this.caseDictionary.Add(val, mostInnerRecursion);
            }

            if (this.caseDictionary.TryGetValue(val, out middleRecursion))
            {
                middleRecursion = GetValueDynamic(mostInnerRecursion);
                this.caseDictionary.Add(val, middleRecursion);
            }

            if (this.caseDictionary.TryGetValue(val + 1 - middleRecursion, out lastRecursion))
            {
                lastRecursion = GetValueDynamic(val + 1 - middleRecursion);
                this.caseDictionary.Add(val, lastRecursion);
            }

            return 1 + lastRecursion;
        }
    }
}
