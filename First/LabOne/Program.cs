﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using LabOne.Base;
using LabOne.ExternalMemory;
using LabOne.HashTable;
using LabOne.Interfaces;
using LabOne._1and4;
using LabOne._3;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace LabOne
{
    class Program
    {
        static void Main(string[] args)
        {
            ConfigureServices(new ServiceCollection());
        }

        static void ConfigureServices(IServiceCollection services)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appConfig.json", optional: false, reloadOnChange: true);
            var configuration = builder.Build();

            var provider = services
                .AddTransient<ISeedService, SeedService>()
                .AddSingleton(configuration)
                .BuildServiceProvider();
            Run(provider);
        }

        static void Run(ServiceProvider p)
        {

            ////RunSorter(p);
            //RunEquation(p, 10);
            //RunEquation(p, 20);
            //RunEquation(p, 30);
            //RunEquation(p, 40);
            //RunEquation(p, 50);
            RunSorter(p, 1000);
            RunSorter(p, 10000);
            RunSorter(p, 20000);
            RunSorter(p, 30000);
            RunSorter(p, 40000);
            Console.ReadKey();

        }

        static void RunEquation(ServiceProvider p, int amount)
        {
            var eq = new Equation();
            var sw = new Stopwatch();
            sw.Start();
            eq.GetValueRecursive(amount);
            Console.WriteLine(sw.Elapsed);
            sw.Restart();
            eq.GetValueDynamic(amount);
            Console.WriteLine(sw.Elapsed);
            Console.WriteLine("Done");
            
        }

        static void RunSorter(ServiceProvider p, int elements)
        {
            var sorter = new ArraySorter(p.GetService<ISeedService>());
            var sw = new Stopwatch();

            sw.Start();
            sorter.SortArraysInSync(elements, elements);
            Console.WriteLine(sw.Elapsed);
            sw.Restart();
            sorter.SortArraysAsync(elements, elements);
            Console.WriteLine(sw.Elapsed);
            Console.WriteLine("Done");
            
        }

        static void DoBenchmark(ServiceProvider p)
        {
            //ArrayBenchmark(new Benchmark<ArrayStructureInMemory>(new Sortable<ArrayStructureInMemory>()), p);
            LinkedListBenchmark(new Benchmark<LinkedListInMemory>(new Sortable<LinkedListInMemory>()), p);
            ArrayBenchmarkEx(new Benchmark<ArrayStructureInExternalMemory>(new Sortable<ArrayStructureInExternalMemory>()), p);
            LinkedListBenchmarkEx(new Benchmark<LinkedListInExternalMemory>(new Sortable<LinkedListInExternalMemory>()), p);
        }

        static void ArrayBenchmark(Benchmark<ArrayStructureInMemory> b, ServiceProvider p)
        {
            var seedService = p.GetService<ISeedService>();
            var list = new List<ArrayStructureInMemory>();
            for (int i = 1; i < 8; i++)
            {
                var inMemoryArray = new ArrayStructureInMemory(seedService, p.GetService<IConfigurationRoot>(), seedService.GenerateAndReturnData(i * 1000).ToArray());
                list.Add(inMemoryArray);
            }
            b.DoBenchMark(list.ToArray());
        }

        static void LinkedListBenchmark(Benchmark<LinkedListInMemory> b, ServiceProvider p)
        {
            var seedService = p.GetService<ISeedService>();
            var list = new List<LinkedListInMemory>();
            for (int i = 1; i < 8; i++)
            {
                var inMemoryLinkedList = new LinkedListInMemory(seedService, seedService.GenerateAndReturnData(i * 1000).ToArray());
                list.Add(inMemoryLinkedList);
            }
            b.DoBenchMark(list.ToArray());
        }

        static void ArrayBenchmarkEx(Benchmark<ArrayStructureInExternalMemory> b, ServiceProvider p)
        {
            var arrayFileStream = new FileStream(Directory.GetCurrentDirectory() + "arrayData.dat", FileMode.OpenOrCreate);
            var list = new List<ArrayStructureInExternalMemory>();
            var seedService = p.GetService<ISeedService>();
            for (int i = 1; i < 8; i++)
            {
                var inExternalMemoryArray = new ArrayStructureInExternalMemory(seedService, arrayFileStream, seedService.GenerateAndReturnData(i * 1000).ToArray());
                list.Add(inExternalMemoryArray);
            }
            b.DoBenchMark(list.ToArray());
        }

        static void LinkedListBenchmarkEx(Benchmark<LinkedListInExternalMemory> b, ServiceProvider p)
        {
            var linkedListFileStream = new FileStream(Directory.GetCurrentDirectory() + "listData.dat", FileMode.OpenOrCreate);
            var list = new List<LinkedListInExternalMemory>();
            var seedService = p.GetService<ISeedService>();
            for (int i = 1; i < 8; i++)
            {
                var inExternalMemoryLinkedList = new LinkedListInExternalMemory(linkedListFileStream, seedService, seedService.GenerateAndReturnData(i * 1000).ToArray());
                list.Add(inExternalMemoryLinkedList);
            }
            b.DoBenchMark(list.ToArray());
        }

        //DoBenchmark(p);
        //var seedService = p.GetService<ISeedService>();
        //var ints = seedService.GenerateAndReturnData();
        //var hashTable = new FixedSizeGenericHashTable<int, int>(ints.Count - 40);

        //var indexer = 0;
        //var keys = Enumerable.Range(0, ints.Count - 1).Select(x => indexer++).ToArray();
        //var itemToSearchOne = keys[25];
        //var itemToSearchTwo = keys[20];
        //indexer = 0;
        //foreach(int i in ints)
        //    hashTable.Add(indexer++, i);
        //hashTable.Find(itemToSearchOne.GetHashCode(), true);
        //hashTable.Find(itemToSearchTwo.GetHashCode(), true);
        //hashTable.AmountOfChains();
        //hashTable.ContainsSpecifiedElementsInChain(itemToSearchOne, itemToSearchTwo);
        //Console.ReadKey();
    }
}
