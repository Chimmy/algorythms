﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using LabOne.Base;
using LabOne.Interfaces;

namespace LabOne
{
    public class Benchmark<T> where T : CollectionBase, IDataStructure
    {
        private Sortable<T> Sortable;
        private Stopwatch sw;

        public Benchmark(Sortable<T> sortable)
        {
            this.Sortable = sortable;
            sw = new Stopwatch();
        }

        public void DoBenchMark(params T[] arr)
        {
            var outfilePath = Directory.GetCurrentDirectory() + "\\Out\\" + arr[0].GetType() + ".csv";
            var fs = new FileStream(outfilePath, FileMode.OpenOrCreate);
            var builder = new StringBuilder();
            builder
                .Append("Iterations;TimeElapsed")
                .Append(Environment.NewLine);
            foreach (var d in arr)
            {
                long result;
                sw.Start();
                Sortable.InsertionSort(d);
                result = sw.ElapsedMilliseconds;
                builder
                    .Append($"{d.Length};{result}")
                    .Append(Environment.NewLine);
            }
            fs.Write(Encoding.ASCII.GetBytes(builder.ToString()));
            fs.Flush();
        }
    }
}
