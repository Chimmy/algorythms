﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LabOne.Interfaces
{
    public interface IDataStructure
    {
        void Swap(int indexOne, int indexTwo);
    }
}
