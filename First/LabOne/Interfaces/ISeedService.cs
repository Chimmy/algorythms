﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LabOne.Interfaces
{
    public interface ISeedService
    {
        IList<int> GenerateAndReturnData(int? specifiedDataCount= null);
    }
}
