﻿using System;
using System.Collections.Generic;
using System.Text;
using LabOne.Base;

namespace LabOne.Interfaces
{
    public interface IMergeSortable<T> where T : CollectionBase, IDataStructure
    {
        void MergeSort(T collection);
    }
}
