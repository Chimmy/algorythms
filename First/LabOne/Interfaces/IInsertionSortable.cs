﻿using System;
using System.Collections.Generic;
using System.Text;
using LabOne.Base;

namespace LabOne.Interfaces
{
    public interface IInsertionSortable<T> where T : CollectionBase, IDataStructure
    {
        void InsertionSort(T collection);
    }
}
