﻿using System;
using System.Collections.Generic;
using System.Linq;
using LabOne.Base;
using LabOne.Interfaces;

namespace LabOne
{
    public class LinkedListInMemory : CollectionBase, IDataStructure
    {
        private readonly ISeedService _seedService;
        private Node Head { get; set; }
        private Node Current;
        private int SizeOfList = 0;
        public override int this[int index]
        {
            get => this.GetAtIndex(index);
            set => SetAtIndex(index, value);
        }

        public override int Length => SizeOfList;

        public LinkedListInMemory(ISeedService seedService, int [] initialData = null)
        {
            _seedService = seedService;
            Fill(initialData);
        }

        private void Fill(int[] initialData = null)
        {
            var data = initialData ?? this._seedService.GenerateAndReturnData().ToArray();
            
            foreach (var i in data)
            {
                Insert(i);
            }
        }

        public void Insert(int item)
        {
            var node = new Node(item);
            this.SizeOfList++;
            if (Head == null)
            {
                Head = node;
                Current = Head;
            }
            else
            {
                Current.Next = node;
                Current = Current.Next;
            }
        }

        private void ResetCurrent()
        {
            Current = Head;
        }

        public List<int> ToList()
        {
            ResetCurrent();
            var list = new List<int>();
            while (Current.Next != null)
            {
                list.Add(Current.Data);
                Current = Current.Next;
            }
            list.Add(Current.Data);
            return list;
        }

        public int GetAtIndex(int index)
        {
            ResetCurrent();
            for (int i = 0; Current.Next != null; i++)
            {
                if (i == index)
                    return Current.Data;
                Current = Current.Next;
            }

            return Current.Data;
        }

        public void SetAtIndex(int index, int data)
        {
            ResetCurrent();
            for (int i = 0; Current.Next != null; i++)
            {
                if (i == index)
                    Current.Data = data;
                Current = Current.Next;
            }

            if (index == this.Length - 1)
            {
                Current.Data = data;
            }
        }

        public void Swap(int fromIndex, int toIndex)
        {
            if(fromIndex == toIndex)
                throw new Exception("Indexes must not be the same");
            ResetCurrent();
            int index = 0;
            Node first = null;
            Node second = null;
            while (Current.Next != null)
            {
                if (index == fromIndex)
                    first = Current;
                if (index == toIndex)
                    second = Current;
                Current = Current.Next;
                index++;
            }
            SwapNodes(first, second);
        }

        private void SwapNodes(Node from, Node to)
        {
            var tmp = from.Data;
            from.Data = to.Data;
            to.Data = tmp;
        }

        private class Node
        {
            public int Data { get; set; }
            public Node Next { get; set; }

            public Node(int data)
            {
                Data = data;
            }
        }
    }
}
