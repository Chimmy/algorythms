﻿using System.Collections;
using System.Linq;
using LabOne.Interfaces;
using Microsoft.Extensions.Configuration;
using CollectionBase = LabOne.Base.CollectionBase;

namespace LabOne
{
    public class ArrayStructureInMemory : CollectionBase, IDataStructure, IEnumerable
    {
        private readonly ISeedService _seedService;
        private int[] data;
        public override int Length => data.Length;
        public override int this[int index]
        {
            get => data[index];
            set => data[index] = value;
        }

        public ArrayStructureInMemory(ISeedService seedServiceService, IConfigurationRoot config, int[] initialData = null)
        {
            _seedService = seedServiceService;
            data = initialData ?? this._seedService.GenerateAndReturnData().ToArray();
        }

        public int[] GetData()
        {
            return this.data;
        }

        public void Swap(int indexOne, int indexTwo)
        {
            var tmp = this.data[indexOne];
            data[indexOne] = data[indexTwo];
            data[indexTwo] = tmp;
        }

        public IEnumerator GetEnumerator()
        {
            foreach (var i in data)
            {
                yield return i;
            }
        }
    }
}
