﻿using System;
using System.Collections.Generic;

namespace LabOne.HashTable
{
    public struct KeyValue<K, V>
    {
        public K Key { get; set; }
        public V Value { get; set; }
    }

    public class FixedSizeGenericHashTable<K, V>
    {
        private readonly int size;
        private readonly LinkedList<KeyValue<K, V>>[] items;

        public FixedSizeGenericHashTable(int size)
        {
            this.size = size;
            items = new LinkedList<KeyValue<K, V>>[size];
        }

        protected int GetArrayPosition(K key)
        {
            int position = key.GetHashCode() % size;
            return Math.Abs(position);
        }

        public V Find(K key, bool displaySearchStatistics = false)
        {
            int position = GetArrayPosition(key);
            int searched = 0;
            LinkedList<KeyValue<K, V>> linkedList = GetLinkedList(position);
            foreach (KeyValue<K, V> item in linkedList)
            {
                searched++;
                if (item.Key.Equals(key))
                {
                    if (displaySearchStatistics)
                    {
                        Console.WriteLine($"Found object after {searched} iterations");
                    }

                    return item.Value;
                }
            }

            return default(V);
        }

        public void AmountOfChains()
        {
            int itemsCount = 0;

            foreach (var item in items)
            {
                if (item != null && item.Count > 0)
                {
                    itemsCount++;
                }
            }
            Console.WriteLine($"HashTable has {itemsCount} chains with actual values");
        }

        public void ContainsSpecifiedElementsInChain(K keyOne, K keyTwo)
        {
            var iteration = 0;
            foreach (var linkedList in items)
            {
                bool containsFirst = false;
                bool containsSecond = false;
                var keyValueOne = new KeyValue<K, V>();
                var keyValueTwo = new KeyValue<K, V>();
                iteration++;
                foreach (var keyValue in linkedList)
                {
                    if (!containsFirst)
                    {
                        containsFirst = keyValue.Key.Equals(keyOne);
                        if (containsFirst)
                            keyValueOne = keyValue;
                    }

                    if (!containsSecond)
                    {
                        containsSecond = keyValue.Key.Equals(keyTwo);
                        if (containsSecond)
                            keyValueTwo = keyValue;
                    }
                }

                if (containsSecond && containsFirst)
                {
                    DisplayKeyValueInfo(keyValueOne, keyValueTwo);
                }
                else
                {
                    Console.WriteLine($"Given keys are not in chain {iteration}");
                }
            }

        }

        private void DisplayKeyValueInfo(KeyValue<K, V> keyValueOne, KeyValue<K, V> keyValueTwo)
        {
            Console.WriteLine($"Given keys exist in the same chain. {Environment.NewLine}" +
                              $"First key = {keyValueOne.Value.GetHashCode()}{Environment.NewLine}" +
                              $"Second key = {keyValueTwo.Value.GetHashCode()}");
        }

        public void Add(K key, V value)
        {
            int position = GetArrayPosition(key);
            LinkedList<KeyValue<K, V>> linkedList = GetLinkedList(position);
            KeyValue<K, V> item = new KeyValue<K, V>() { Key = key, Value = value };
            linkedList.AddLast(item);
        }

        public void Remove(K key)
        {
            int position = GetArrayPosition(key);
            LinkedList<KeyValue<K, V>> linkedList = GetLinkedList(position);
            bool itemFound = false;
            KeyValue<K, V> foundItem = default(KeyValue<K, V>);
            foreach (KeyValue<K, V> item in linkedList)
            {
                if (item.Key.Equals(key))
                {
                    itemFound = true;
                    foundItem = item;
                }
            }

            if (itemFound)
            {
                linkedList.Remove(foundItem);
            }
        }

        protected LinkedList<KeyValue<K, V>> GetLinkedList(int position)
        {
            LinkedList<KeyValue<K, V>> linkedList = items[position];
            if (linkedList == null)
            {
                linkedList = new LinkedList<KeyValue<K, V>>();
                items[position] = linkedList;
            }

            return linkedList;
        }
    }
}
