﻿using System;
using System.Collections.Generic;
using System.Text;
using LabOne.Base;
using LabOne.Interfaces;

namespace LabOne.Extensions
{
    static class StructureExtensions
    {
        public static T Swap<T>(this T structure, int indexOne, int indexTwo) where T : CollectionBase, IDataStructure
        {
            var tmp = structure[indexOne];
            structure[indexOne] = structure[indexTwo];
            structure[indexTwo] = tmp;
            return structure;
        }
    }
}
