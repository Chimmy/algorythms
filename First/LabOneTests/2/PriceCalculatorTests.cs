﻿using System;
using System.Collections.Generic;
using System.Text;
using LabOneTests.TestSpecificInterfaces;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace LabOneTests._4
{
    public class ProfitCalculatorTests : LabTestBase
    {
        private readonly IInitializeHelper _initializeHelper;
        public ProfitCalculatorTests()
        {
            _initializeHelper = Provider.GetService<IInitializeHelper>();
        }

        [Fact]
        public void ProfitCalculator_MaxProfit_Works()
        {
            var input = new int[] { 14,10,13,12,14 };
            var calculator = this._initializeHelper.GetProfitCalculator();

            var output = calculator.MaxProfit(2, input);
            Assert.True(output == 5);
        }

    }
}
