﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using LabOne;
using LabOne.Base;
using LabOne.ExternalMemory;
using LabOne.Interfaces;
using LabOneTests.TestSpecificInterfaces;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace LabOneTests.Sort
{
    public class MergeSortTests : LabTestBase
    {
        private readonly IInitializeHelper _initializeHelper;

        public MergeSortTests()
        {
            _initializeHelper = Provider.GetService<IInitializeHelper>();
        }

        [Fact]
        public void MergeSort_WorksWithInMemoryArray()
        {
            var arr = _initializeHelper.GetInMemoryArray();
            var sortable = new Sortable<ArrayStructureInMemory>();
            sortable.MergeSort(arr);

            for (int i = 0; i < arr.Length - 1; i++)
            {
                Assert.True(arr[i] < arr[i + 1]);
            }
        }

        [Fact]
        public void MergeSort_WorksWithInMemoryLinkedList()
        {
            var arr = _initializeHelper.GetInMemoryLinkedList();
            var sortable = new Sortable<LinkedListInMemory>();
            sortable.MergeSort(arr);
            for (int i = 0; i < arr.Length - 1; i++)
            {
                Assert.True(arr[i] <= arr[i + 1]);
            }
        }

        [Fact]
        public void MergeSort_WorksWithExternalMemoryArray()
        {
            var arr = _initializeHelper.GetInExternalMemoryArray();
            var sortable = new Sortable<ArrayStructureInExternalMemory>();
            sortable.MergeSort(arr);
            for (int i = 0; i < arr.Length - 1; i++)
            {
                Assert.True(arr[i] <= arr[i + 1]);
            }
            arr.Dispose();
        }

        [Fact]
        public void MergeSort_WorksWithExternalMemoryLinkedList()
        {
            var arr = _initializeHelper.GetInExternalMemoryLinkedList();
            var sortable = new Sortable<LinkedListInExternalMemory>();
            sortable.MergeSort(arr);
            for (int i = 0; i < arr.Length - 1; i++)
            {
                Assert.True(arr[i] <= arr[i + 1]);
            }
            arr.Dispose();
        }
    }
}
