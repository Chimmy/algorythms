﻿using System;
using System.Collections.Generic;
using System.Text;
using LabOne;
using LabOne.Base;
using LabOne.ExternalMemory;
using LabOneTests.TestSpecificInterfaces;
using Xunit;
using Microsoft.Extensions.DependencyInjection;

namespace LabOneTests.Sort
{
    public class InsertionSortTests : LabTestBase
    {
        private readonly IInitializeHelper _initializeHelper;

        public InsertionSortTests()
        {
            _initializeHelper = Provider.GetService<IInitializeHelper>();
        }

        [Fact]
        public void InsertionSort_WorksWithInMemoryArray()
        {
            var arr = _initializeHelper.GetInMemoryArray();
            var sortable = new Sortable<ArrayStructureInMemory>();
            sortable.InsertionSort(arr);

            for (int i = 0; i < arr.Length - 1; i++)
            {
                Assert.True(arr[i] < arr[i + 1]);
            }
        }

        [Fact]
        public void InsertionSort_WorksWithInMemoryLinkedList()
        {
            var arr = _initializeHelper.GetInMemoryLinkedList();
            var sortable = new Sortable<LinkedListInMemory>();
            sortable.InsertionSort(arr);

            for (int i = 0; i < arr.Length - 1; i++)
            {
                Assert.True(arr[i] < arr[i + 1]);
            }
        }

        [Fact]
        public void InsertionSort_WorksWithExternalMemoryArray()
        {
            var arr = _initializeHelper.GetInExternalMemoryArray();
            var sortable = new Sortable<ArrayStructureInExternalMemory>();
            sortable.InsertionSort(arr);

            for (int i = 0; i < arr.Length - 1; i++)
            {
                Assert.True(arr[i] <= arr[i + 1]);
            }
            arr.Dispose();
        }

        [Fact]
        public void InsertionSort_WorksWithExternalMemoryLinkedList()
        {
            var arr = _initializeHelper.GetInExternalMemoryLinkedList();
            var sortable = new Sortable<LinkedListInExternalMemory>();
            sortable.InsertionSort(arr);

            for (int i = 0; i < arr.Length - 1; i++)
            {
                Assert.True(arr[i] <= arr[i + 1]);
            }
            arr.Dispose();
        }
    }
}
