﻿using System;
using System.Collections.Generic;
using System.Text;
using LabOne;
using LabOne.Interfaces;
using LabOneTests.TestSpecificInterfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace LabOneTests.Structures
{
    public class LinkedListInInternalMemoryTests : LabTestBase
    {
        private readonly IInitializeHelper _initializeHelper;
        public LinkedListInInternalMemoryTests()
        {
            this._initializeHelper = Provider.GetService<IInitializeHelper>();
        }

        private ISeedService GetSeedService()
        {
            return Provider.GetService<ISeedService>();
        }

        [Fact]
        public void LinkedList_InsertsData()
        {
            var linkedList = _initializeHelper.GetInExternalMemoryLinkedList();
            Assert.NotEmpty(linkedList);
            Assert.True(linkedList.Length == 50);

            linkedList.Insert(50);
            Assert.True(linkedList.Length == 51);
        }

        [Fact]
        public void LinkedList_ReturnsCorrectAtIndex()
        {
            var linkedList = _initializeHelper.GetInExternalMemoryLinkedList();
            var expected = linkedList[1];
            linkedList[2] = linkedList[1];
            linkedList[40] = linkedList[1];
            Assert.Equal(linkedList[2], expected);
            Assert.Equal(linkedList[40], expected);
        }

        [Fact]
        public void LinkedList_SwapsValues()
        {
            var data = GetSeedService().GenerateAndReturnData();
            var linkedList = _initializeHelper.GetInExternalMemoryLinkedList();
            var expected = data[2];
            foreach (var d in data)
            {
                linkedList.Insert(d);
            }

            linkedList.Swap(0, 2);

            Assert.Equal(linkedList.GetAtIndex(0), expected);
        }
    }
}
