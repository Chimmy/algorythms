﻿using System;
using System.Collections.Generic;
using System.Text;
using LabOne;
using LabOne.Interfaces;
using LabOneTests.TestSpecificInterfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace LabOneTests.Structures
{
    public class ArrayInInternalMemoryTests : LabTestBase
    {
        private readonly IInitializeHelper _initializeHelper;
        public ArrayInInternalMemoryTests()
        {
            _initializeHelper = Provider.GetService<IInitializeHelper>();
        }

        [Fact]
        public void ArrayStructure_SwapsElements()
        {
            var arrayStructure = _initializeHelper.GetInMemoryArray();
            var arr = arrayStructure.GetData();
            int a = arr[0];
            int b = arr[1];

            arrayStructure.Swap(0, 1);
            arr = arrayStructure.GetData();

            Assert.Equal(arr[0], b);
            Assert.Equal(arr[1], a);
        }

        [Fact]
        public void ArrayStructure_GetsSettingsFromConfig()
        {
            var arrayStructure = _initializeHelper.GetInMemoryArray();
            var arr = arrayStructure.GetData();
            var config = Provider.GetService<IConfigurationRoot>();
            var expected = int.Parse(config["amountOfDataToGenerate"]);

            Assert.Equal(arr.Length, expected);
        }

    }
}
