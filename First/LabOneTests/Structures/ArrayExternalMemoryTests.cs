﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using LabOne;
using LabOne.ExternalMemory;
using LabOne.Interfaces;
using LabOneTests.TestSpecificInterfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace LabOneTests.Structures
{
    public class ArrayExternalMemoryTests : LabTestBase
    {
        private readonly IInitializeHelper _initializeHelper;
        public ArrayExternalMemoryTests()
        {
            _initializeHelper = Provider.GetService<IInitializeHelper>();
        }

        [Fact]
        public void ArrayStructureInExternalMemory_GetData_ShouldReturnData()
        {
            var arr = _initializeHelper.GetInExternalMemoryArray();
            Assert.NotEmpty(arr.GetData());
        }
    }
}
