using System;
using Xunit;
using Xunit.Abstractions;
using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using LabOne;
using LabOne.Interfaces;
using LabOneTests.TestSpecificInterfaces;

namespace LabOneTests
{
    public class LabTestBase
    {
        public ServiceProvider Provider;
        public LabTestBase()
        {
            ConfigureServices(new ServiceCollection());
        }

        public void ConfigureServices(ServiceCollection services)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appConfig.json", optional: false, reloadOnChange: true);
            var configuration = builder.Build();

            this.Provider = services
                .AddTransient<ISeedService, SeedService>()
                .AddTransient<IInitializeHelper, InitializeHelper>()
                .AddSingleton(configuration)
                .BuildServiceProvider();
        }

    }
}
