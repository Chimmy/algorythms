﻿using System;
using Xunit;
using Xunit.Extensions;
using Xunit.Abstractions;
using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using LabOne;
using LabOne.Interfaces;

namespace LabOneTests.Services
{
    public class SeedTests : LabTestBase
    {
        private readonly ISeedService _seedService;


        public SeedTests()
        {
            this._seedService = new SeedService(base.Provider.GetService<IConfigurationRoot>());
        }

        [Fact]
        public void SeedService_ProvidesDataArray()
        {
            var result = this._seedService.GenerateAndReturnData();
            Assert.NotEmpty(result);
        }
    }
}
