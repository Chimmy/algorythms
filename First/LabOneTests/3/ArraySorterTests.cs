﻿using System;
using System.Collections.Generic;
using System.Text;
using LabOne.Interfaces;
using LabOneTests.TestSpecificInterfaces;
using Microsoft.Extensions.DependencyInjection;
using Xunit;


namespace LabOneTests._3
{
    public class ArraySorterTests : LabTestBase
    {
        private readonly IInitializeHelper _initializeHelper;
        public ArraySorterTests()
        {
            _initializeHelper = Provider.GetService<IInitializeHelper>();
        }

        [Fact]
        public void SortsArraysAsync_Works()
        {
            var sorter = this._initializeHelper.GetArraySorter();

            var output = sorter.SortArraysAsync(10, 10);

            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10 - 1; j++)
                {
                    Assert.True(output[i][j + 1] >= output[i][j]);
                }
            }
        }

        [Fact]
        public void SortsArraysInSync_Works()
        {
            var sorter = this._initializeHelper.GetArraySorter();

            var output = sorter.SortArraysInSync(10, 10);

            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10 - 1; j++)
                {
                    Assert.True(output[i][j + 1] >= output[i][j]);
                }
            }
        }
    }
}
