﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using LabOne;
using LabOne.ExternalMemory;
using LabOne.Interfaces;
using LabOne._3;
using LabOne._4;
using LabOneTests.TestSpecificInterfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace LabOneTests
{
    public class InitializeHelper : LabTestBase, IInitializeHelper
    {
        public ArrayStructureInMemory GetInMemoryArray()
        {
            return new ArrayStructureInMemory(Provider.GetService<ISeedService>(), Provider.GetService<IConfigurationRoot>());
        }

        public LinkedListInMemory GetInMemoryLinkedList()
        {
            return new LinkedListInMemory(Provider.GetService<ISeedService>());
        }

        public ArrayStructureInExternalMemory GetInExternalMemoryArray()
        {
            return new ArrayStructureInExternalMemory(Provider.GetService<ISeedService>(), new FileStream(GetOutFilePath(), FileMode.OpenOrCreate));
        }

        public LinkedListInExternalMemory GetInExternalMemoryLinkedList()
        {
            return new LinkedListInExternalMemory(new FileStream(GetOutFilePath(), FileMode.OpenOrCreate), Provider.GetService<ISeedService>());
        }

        public string GetOutFilePath()
        {
            return Directory.GetCurrentDirectory() + "\\Out\\datafile.dat";
        }

        public ProfitCalculator GetProfitCalculator()
        {
            return new ProfitCalculator(Provider.GetService<ISeedService>());
        }

        public ArraySorter GetArraySorter()
        {
            return new ArraySorter(Provider.GetService<ISeedService>());
        }
    }
}
