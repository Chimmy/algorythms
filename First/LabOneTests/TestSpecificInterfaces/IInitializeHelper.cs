﻿using System;
using System.Collections.Generic;
using System.Text;
using LabOne;
using LabOne.ExternalMemory;
using LabOne._3;
using LabOne._4;

namespace LabOneTests.TestSpecificInterfaces
{
    public interface IInitializeHelper
    {
        string GetOutFilePath();
        ArrayStructureInMemory GetInMemoryArray();
        LinkedListInMemory GetInMemoryLinkedList();
        ArrayStructureInExternalMemory GetInExternalMemoryArray();
        LinkedListInExternalMemory GetInExternalMemoryLinkedList();
        ProfitCalculator GetProfitCalculator();
        ArraySorter GetArraySorter();
    }
}
